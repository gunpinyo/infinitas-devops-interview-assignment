## Overview

This repository contains a proof of concept about how to setup and manage a
simple Kubernetes cluster in a cloud that enforces GitOps and
infrastructure-as-code principles. 

This file will be written in the demonstration format where the reader should
be able to replicate the instruction easily.
<!-- Trivial: the pronoun "we" will be used to refer to both myself writing demo and -->
<!-- the reader who try to repricate the demo, whereas the pronoun "I" will be used -->
<!-- to refer to the case of myself specificly. -->

Regarding the cloud provider, we use [Google Kubernetes Engine (a.k.a. GKE)](
https://cloud.google.com/kubernetes-engine/docs) to host the k8s cluster.
.

Regarding the GitOps principle, we use this GitLab repository to host the
configuration; then, we use [Argo CD](https://argo-cd.readthedocs.io) to
synchronize our k8s cluster with this repository.

Regarding the infrastructure-as-code principle (a.k.a. IAC), we use
[Terraform](https://www.terraform.io) to create the k8s cluster and install
Argo CD on it.

Regarding the code quality control (TODO: future work), we use [SonarQube](
https://www.sonarsource.com/products/sonarqube).

Regarding the secret management, we will eventually use [Hashicorp Vault](
  https://www.vaultproject.io) with [`argocd-vault-plugin`](
  https://argocd-vault-plugin.readthedocs.io/en/stable/). But for this MVP demo,
  we will just manually assign environment variables in a working terminal.

Useful resources:
  - https://developer.hashicorp.com/terraform/tutorials/kubernetes/gke
  - https://www.harness.io/blog/gitops-secrets
  - https://betterprogramming.pub/how-to-set-up-argo-cd-with-terraform-to-implement-pure-gitops-d5a1d797926a
  - https://www.youtube.com/watch?v=MeU5_k9ssrs


## Step 1: bootstrapping GKE

I will use my personal Google cloud account to host the Kubernetes cluster.
This can be roughly summarised as follows.
  - Register a Google Cloud account.
  - Create a new project in Google Cloud.
    (In my particular case, it is named "gunp-infinitas-demo".)
  - Enable Kubernetes Engine API.


## Step 2: installing relevant programs in our local machine

We install and initialize all programs in our local machine (in my case it is
Ubuntu 22.04).

### install and initialize Google Cloud SDK
We install [gcloud SDK](https://cloud.google.com/sdk/gcloud) in the local
machine using the instruction [here](https://cloud.google.com/sdk/docs/install).
The last step of the instruction above is executing `gcloud init` which will
interactively ask us to authenticate into the Google Cloud service so Terraform
can later operate in our behalf. In this and later sessions, we can login by
executing `gcloud auth application-default login`.


### install Terraform
We install Terraform in the local machine by follow the instruction at
https://developer.hashicorp.com/terraform/downloads
.
```
  wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
  echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    sudo tee /etc/apt/sources.list.d/hashicorp.list
  sudo apt update && sudo apt install terraform
```

### install `kubectl`
please follow the instruction at
https://kubernetes.io/docs/tasks/tools/install-kubectl-linux
```
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
  curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
  echo "$(<kubectl.sha256)  kubectl" | sha256sum --check
  sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

## Step 3: bootstrapping Terraform

### use a template for Terraform

use Terraform template form its official tutorial
This step is inspired by [this Hashicorp official tutorial](
https://developer.hashicorp.com/terraform/tutorials/kubernetes/gke).

We start configuring the Terraform by borrowing files `gke.tf`, `vpc.tf`,
`versions.tf`, and `terraform.tfvars` from [this GitHub repository](
https://github.com/hashicorp/learn-terraform-provision-gke-cluster) to this
repository in folder `terraform`. Then, we change variables in
`terraform.tfvars` accordingly.

### change Terraform state backend to Google Cloud storage

When we later initialize our Terraform, it will create a file named
`terraform.tfstate` to keep track its actual state (to be compared against its
desired state written in folder `terraform` mentioned above); by default, this
file will be stored locally which is undesirable because of:
  - *Security*, this file would contains unencrypted sensitive data this has
    indeed been an open [issue](
    https://github.com/hashicorp/terraform/issues/516) for 9 years!, and
  - *Fragmentation*, the state here shouldn't have multiple copies on multiple
    machines.

To fix this problem we change the location of this file form our local machine
to a dedicate backend such as google *Google Cloud storage*.

We initialize the storage by executing the following command. Please note that,
`gunp-infinitas-demo` is the id of a Google Cloud project that I have created
for this demo.
```
PROJECT_ID=gunp-infinitas-demo
gsutil mb gs://terraform-backend-$PROJECT_ID
```
Then, we tell add the following in file `versions.tf`.
```
backend "gcs" {
  bucket = "terraform-backend-gunp-infinitas-demo"
  prefix = "argocd-terraform"
}
```

### initializing the Terraform
To initialize the Terraform, we change the working directory as the folder
`terraform` in this repository and execute `terraform init`.
![`images/terraform-init.png`](images/terraform-init.png)

To check that our configuration is valid, we execute `terraform validate`.  To
see that what Terraform will do (according to current configuration), we execute
`terraform plan`.

If everything is alright, then we can apply by execute `terraform apply`.

## Step 4: install Argo CD in the k8s cluster

TODO:

## Step 5: synchronise Argo CD to GitLab

TODO:

## Step 6: code quality control with SonarQube

TODO: future work

## Step 7: secret management with Hashicorp Vault

TODO: future work

<!---

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
--->
