## Step 1: bootstrapping the Kubernetes cluster

This step is mainly inspired by [this blog post](
https://www.hashicorp.com/blog/terraform-adds-support-for-gke-autopilot)

For the replication purpose, I would like to clarify that local machine that
connect to the cluster is using Ubuntu 22.04.

### initialize the Google cloud account and project ID
I will use my personal Google cloud account to host the Kubernetes cluster.
This can be roughly summarised as follows.
  - Register a Google Cloud account.
  - Create a new project in Google Cloud.
    (In my particular case, it is named "gunp-infinitas-demo".)
  - Enable Kubernetes Engine API.

### install and initialize Google Cloud SDK
We install [gcloud SDK](https://cloud.google.com/sdk/gcloud) in the local
machine using the instruction [here](https://cloud.google.com/sdk/docs/install).
The last step of the instruction above is executing `gcloud init` which will
interactively ask us to authenticate into the Google Cloud service so Terraform
can later operate in our behalf. In this and later sessions, we can login by
executing `gcloud auth application-default login`.

### install `kubectl`
please follow the instruction at
https://kubernetes.io/docs/tasks/tools/install-kubectl-linux
```
  curl -LO "https://dl.k8s.io/release/$(curl -L -s \
    https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
  curl -LO "https://dl.k8s.io/$(curl -L -s \
    https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
  echo "$(<kubectl.sha256)  kubectl" | sha256sum --check
  sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

According to [this blog post](
https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke)
we also need to install an additional plug-in.
```
gcloud components install gke-gcloud-auth-plugin
```


### install Terraform
We install Terraform in the local machine by follow the instruction at
[here](https://developer.hashicorp.com/terraform/downloads).
```
  wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
  echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    sudo tee /etc/apt/sources.list.d/hashicorp.list
  sudo apt update && sudo apt install terraform
```

### use a template for Terraform

<!-- Use Terraform template form its official tutorial -->
<!-- This step is inspired by [this Hashicorp official tutorial]( -->
<!-- https://developer.hashicorp.com/terraform/tutorials/kubernetes/gke). -->

<!-- We start configuring the Terraform by borrowing files `gke.tf`, `vpc.tf`, -->
<!-- `versions.tf`, and `terraform.tfvars` from [this GitHub repository]( -->
<!-- https://github.com/hashicorp/learn-terraform-provision-gke-cluster) to this -->
<!-- repository in folder `terraform`. Then, we change variables in -->
<!-- `terraform.tfvars` accordingly. -->

Inspired by [this blog post](https://betterprogramming.pub/
how-to-set-up-argo-cd-with-terraform-to-implement-pure-gitops-d5a1d797926a), we
copy files `terraform/main.tf` and `terraform/ver.tf` form [this repo](
https://github.com/bharatmicrosystems/argo-cd-example) then modify them
accordingly.

We also copy file `outputs.tf` from [this repo](
https://github.com/hashicorp/learn-terraform-provision-gke-cluster.) and modify
accordingly.

### use autopilot mode of GKE

The template above uses standard mode of GKE, we change to use the autopilot
mode instead by modify the files `terraform/main.tf` according to [this blog
post]( https://www.hashicorp.com/blog/terraform-adds-support-for-gke-autopilot).
Unfortunately, there is a bug similar to [this GitHub issue](
https://github.com/hashicorp/terraform-provider-google/issues/10782) so we apply
[this workaround](https://github.com/hashicorp/terraform-provider-google/
issues/10782#issuecomment-1024488630).

### change Terraform state backend to Google Cloud storage

When we later initialize our Terraform, it will create a file named
`terraform.tfstate` to keep track its actual state (to be compared against its
desired state written in folder `terraform` mentioned above); by default, this
file will be stored locally which is undesirable because of:
  - *Security*, this file would contains unencrypted sensitive data this has
    indeed been an open [issue](
    https://github.com/hashicorp/terraform/issues/516) for 9 years!, and
  - *Fragmentation*, the state here shouldn't have multiple copies on multiple
    machines.

To fix this problem we change the location of this file form our local machine
to a dedicate backend such as google *Google Cloud storage*.

We initialize the storage by executing the following command. Please note that,
`gunp-infinitas-demo` is the id of a Google Cloud project that I have created
for this demo.
```
PROJECT_ID=gunp-infinitas-demo
gsutil mb gs://terraform-backend-$PROJECT_ID
```
Then, we tell add the following in file `versions.tf`.
```
backend "gcs" {
  bucket = "terraform-backend-gunp-infinitas-demo"
  prefix = "argocd-terraform"
}
```

### initializing the Terraform
To initialize the Terraform, we change the working directory as the folder
`terraform` in this repository and execute `terraform init`.
![`images/terraform-init.png`](images/terraform-init.png)

To check that our configuration is valid, we execute the following command.
```
terraform validate
```

To see that what Terraform will do (according to current configuration), we
execute the following command.
```
terraform plan
```

If everything is alright, then we can apply by execute the following command.
```
terraform apply
```
This should create a cluster in GKE for us.

Finally, we configure `kubectl` to connect with our cluster using the
following command.
```
gcloud container clusters get-credentials \
  $(terraform output -raw kubernetes_cluster_name) \
  --region $(terraform output -raw region)
```

## Step 2: deploying MongoDB

This step is mainly inspired by [this blog post](https://www.mongodb.com/blog/post/
run-secure-containerized-mongodb-deployments-using-the-mongo-db-community-kubernetes-oper).

