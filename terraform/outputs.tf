# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

# originally, this file is copied form file `outputs.tf`
# (in repo https://github.com/hashicorp/learn-terraform-provision-gke-cluster)
# then later be modified to fit this repository

output "region" {
  value       = var.region
  description = "GCloud Region"
}

output "project_id" {
  value       = var.project_id
  description = "GCloud Project ID"
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.main.name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = google_container_cluster.main.endpoint
  description = "GKE Cluster Host"
}

