# originally, this file is copied from file `terraform/main.tf`
# (in repo https://github.com/bharatmicrosystems/argo-cd-example)
# then later be modified to fit this repository; in particular,
#   - we will use autopilot mode of GKE (see
#       https://www.hashicorp.com/blog/terraform-adds-support-for-gke-autopilot)
#   - because of the autopilot, the cluster is changed from zonal to regional

terraform {
  required_version = ">= 0.14"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.47.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
  backend "gcs" {
    bucket = "terraform-backend-gunp-infinitas-demo"
    prefix = "argocd-terraform"
  }
}

provider "google" {
  project               = var.project_id
  region                = var.region
}

resource "google_service_account" "main" {
  account_id            = "${var.cluster_name}-sa"
  display_name          = "GKE Cluster ${var.cluster_name} Service Account"
}

resource "google_container_cluster" "main" {
  name                  = "${var.cluster_name}"
  location              = var.region

  # see https://github.com/hashicorp/terraform-provider-google/
  #       issues/10782#issuecomment-1024488630
  ip_allocation_policy {
  }
  
  # Enabling Autopilot for this cluster
  enable_autopilot      = true

  timeouts {
    create              = "30m"
    update              = "40m"
  }
}

resource "time_sleep" "wait_30_seconds" {
  depends_on            = [google_container_cluster.main]
  create_duration       = "30s"
}

module "gke_auth" {
  depends_on            = [time_sleep.wait_30_seconds]
  source = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  project_id            = var.project_id
  cluster_name          = google_container_cluster.main.name
  location              = var.region
  use_private_endpoint  = false
}

provider "kubectl" {
  host                   = module.gke_auth.host
  cluster_ca_certificate = module.gke_auth.cluster_ca_certificate
  token                  = module.gke_auth.token
  load_config_file       = false
}

# data "kubectl_file_documents" "namespace" {
#   content = file("../manifests/argocd/namespace.yaml")
# } 

# data "kubectl_file_documents" "argocd" {
#   content = file("../manifests/argocd/install.yaml")
# }

# resource "kubectl_manifest" "namespace" {
#   count     = length(data.kubectl_file_documents.namespace.documents)
#   yaml_body = element(data.kubectl_file_documents.namespace.documents,
#                       count.index)
#   override_namespace = "argocd"
# }

# resource "kubectl_manifest" "argocd" {
#   depends_on = [
#     kubectl_manifest.namespace,
#   ]
#   count     = length(data.kubectl_file_documents.argocd.documents)
#   yaml_body = element(data.kubectl_file_documents.argocd.documents,
#                       count.index)
#   override_namespace = "argocd"
# }

# data "kubectl_file_documents" "my-nginx-app" {
#   content = file("../manifests/argocd/my-nginx-app.yaml")
# }

# resource "kubectl_manifest" "my-nginx-app" {
#   depends_on = [
#     kubectl_manifest.argocd,
#   ]
#   count     = length(data.kubectl_file_documents.my-nginx-app.documents)
#   yaml_body = element(data.kubectl_file_documents.my-nginx-app.documents,
#                       count.index)
#   override_namespace = "argocd"
# }
