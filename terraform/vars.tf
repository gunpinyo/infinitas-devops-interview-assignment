# originally, this file is copied from file `terraform/vars.tf`
# (in repo https://github.com/bharatmicrosystems/argo-cd-example)
# then later be modified to fit this repository

variable "project_id" {
  type        = string
  description = "project id"
  default     = "gunp-infinitas-demo"
}

variable "cluster_name" {
  type        = string
  description = "cluster name"
  default     = "k8s-cluster"
}

variable "region" {
  type        = string
  description = "cluster region"
  default     = "asia-southeast1"
}
